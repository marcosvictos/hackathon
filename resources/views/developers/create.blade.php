@extends('layouts.app')

@section('content')
<div class="panel panel-default col-md-8 col-md-offset-2    ">
<div class="panel-heading"><b>Register Form</b></div>
<div class="panel-body">
    <div class="row">
         @if (session('error'))
             <div class="alert alert-danger">{{session('error')}}</div>
         @endif
    </div>

    <div class="row">
         @if (session('success'))
             <div class="alert alert-success">{{session('success')}}</div>
         @endif
    </div>
     
     {!! Form::model($developer,['route' => 'developer.create','method'=>'post','enctype'=>'multipart/form-data']) !!}
         @include('developers/developer-form')
     {!! Form::close() !!}
</div>
</div>
@endsection
