<div class="form-group required col-md-6 col-lg-6 col-sm-6 col-xs-12">
    {!! Form::label('name', 'Full Names',['class'=>'control-label']) !!}
    {!! Form::text('name',NULL,['class'=>'form-control']) !!}

    @if ($errors->has('name'))
        <span class="help-block">
            <b class="text-danger">{{ $errors->first('name') }}</b>
        </span>
    @endif
</div>

<div class="form-group required col-md-6 col-lg-6 col-sm-6 col-xs-12">
    {!! Form::label('email', 'Email',['class'=>'control-label']) !!}
    {!! Form::text('email',NULL,['class'=>'form-control']) !!}

    @if ($errors->has('email'))
        <span class="help-block">
            <b class="text-danger">{{ $errors->first('email') }}</b>
        </span>
    @endif
</div>
<div class="form-group required col-md-6 col-lg-6 col-sm-6 col-xs-12">
    {!! Form::label('type', 'Registration Type',['class'=>'control-label']) !!}
    {!! Form::select('type',['individual'=>'Individual','group'=>'Group'],NULL,['class'=>'form-control']) !!}

    @if ($errors->has('type'))
        <span class="help-block">
            <b class="text-danger">{{ $errors->first('type') }}</b>
        </span>
    @endif
</div>
<div class="form-group required col-md-6 col-lg-6 col-sm-6 col-xs-12">
    {!! Form::label('course', 'Course',['class'=>'control-label']) !!}
    {!! Form::text('course',NULL,['class'=>'form-control']) !!}

    @if ($errors->has('course'))
        <span class="help-block">
            <b class="text-danger">{{ $errors->first('course') }}</b>
        </span>
    @endif
</div>
<div class="form-group required col-md-6 col-lg-6 col-sm-6 col-xs-12">
    {!! Form::label('year_of_study', 'Year of study',['class'=>'control-label']) !!}
    {!! Form::text('year_of_study',NULL,['class'=>'form-control']) !!}

    @if ($errors->has('year_of_study'))
        <span class="help-block">
            <b class="text-danger">{{ $errors->first('year_of_study') }}</b>
        </span>
    @endif
</div>
<div class="form-group required col-md-6 col-lg-6 col-sm-6 col-xs-12">
    {!! Form::label('no_of_people', 'Number of people',['class'=>'control-label']) !!}
    {!! Form::text('no_of_people',NULL,['class'=>'form-control']) !!}

    @if ($errors->has('no_of_people'))
        <span class="help-block">
            <b class="text-danger">{{ $errors->first('no_of_people') }}</b>
        </span>
    @endif
</div>
<div class="form-group required col-md-6 col-lg-6 col-sm-6 col-xs-12">
    {!! Form::label('photo', 'Profile Picture',['class'=>'control-label']) !!}
    {!! Form::file('photo',NULL,['class'=>'form-control']) !!}

    @if ($errors->has('photo'))
        <span class="help-block">
            <b class="text-danger">{{ $errors->first('photo') }}</b>
        </span>
    @endif
</div>

<div class="form-group col-md-12">
    {!! Form::submit('Submit',['class'=>'btn btn-success']) !!}
</div>