@extends('layouts.app')

@section('content')
<div class="container">
<table id="developers" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Full Names</th>
                <th>Email</th>
                <th>Registration Type</th>
                <th>Course</th>
                <th>Year of Study</th>
                <th>No of People</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Full Names</th>
                <th>Email</th>
                <th>Registration Type</th>
                <th>Course</th>
                <th>Year of Study</th>
                <th>No of People</th>
            </tr>
        </tfoot>
    </table>
</div>
@push('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function(){
    
    $('#developers').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "/devs/get",
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    } );
});

</script>
@endpush
@endsection