<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Developer;
use App\SSP;
use Carbon\Carbon;
use Image;

class DeveloperController extends Controller
{
    //

    public function create(Request $request){
        return view('developers/create',['developer'=>[]]);
    }
    public function store(Request $request){
        Validator::make($request->all(),[
            // 'name'=>'required',
            // 'email'=>'required',
            // 'year_of_study'=>'required',
            'photo'=>'required|mimes:jpeg,png'
        ])->validate();
        $img = Image::make($request->file('photo'));
        $img->fit(300);
        $filename = Carbon::now()->timestamp.'.png';
        if($img->save(storage_path().'/app/'.$filename)):
            $input = $request->except('_token');
            $input['photo']=$filename;

            $developer = Developer::create($input);

            if($developer):
                return redirect()->back()->with('success',"Registration completed successfully.");
            else:
                return redirect()->back()->with('error',"Registration failed, please try again.");
            endif;
        else:
            return redirect()->back()->with('error',"Ooops! Something went wrong, please try again.");
        endif;
    }
    public function view(Request $request){

    }

    public function index(){
        return view('developers.index');
    }

    public function get(Request $request){
        // DB table to use
        $table = 'developers';
        
        // Table's primary key
        $primaryKey = 'id';

        $columns = array(
            ['db'=>'name','dt'=>0],
            ['db'=>'email','dt'=>1],
            ['db'=>'type','dt'=>2],
            ['db'=>'course','dt'=>3],
            ['db'=>'year_of_study','dt'=>4],
            ['db'=>'no_of_people','dt'=>5]
        );

        $sql_details = array(
            'user' => env('DB_USERNAME'),
            'pass' => env('DB_PASSWORD'),
            'db'   => env('DB_DATABASE'),
            'host' => env('DB_HOST')
        );

        return json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
       
    }
}
