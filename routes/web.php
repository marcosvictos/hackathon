<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dev/create', 'DeveloperController@create')->name('dev-create');
Route::post('/dev/store', 'DeveloperController@store')->name('developer.create');
Route::get('/devs', 'DeveloperController@index')->name('developer.index');
Route::get('/devs/get', 'DeveloperController@get')->name('developer.get');
