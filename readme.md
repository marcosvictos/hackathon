## Hackathon Registration


#Installation instructions
- **[PHP >= 7.0.0]

- [Download or clone the folder from git repository] (git@gitlab.com:marcosvictos/hackathon.git) (https://gitlab.com/marcosvictos/hackathon.git)

- [Copy the folder to the server root directory]
- [Run composer install]
- [Create a database named hackareg]
- [Go to the project root folder and open .env file.]
- [If it is not present then create one and copy the contents from .env.example]
- [Edit the following config attributes in .env accordingly 
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=hackareg
        DB_USERNAME=root
        DB_PASSWORD= ]
- [Run php artisan migrate]
- [Serve the app on the browser]